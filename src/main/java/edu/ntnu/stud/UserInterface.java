package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/**
 * 
 * <p>This is the UI class to display depatures.<br></p>
 * <p>Goal: display departures in a user friendly way. And get the user input</p>

 * @author Jonas Reiher
 * @version 1.0
 * @since 0.4
 * 
 */
public class UserInterface {

  // Scanner
  private final Scanner scanner = new Scanner(System.in);

  // Departure register
  private TrainDepartureRegister departureRegister;

  // Train register
  private TrainRegister trainRegister;

  // boolean varaible to check if the user wants to exit the program
  private boolean exitProgram;

  private LocalTime systemTime;
  

  // Train and TrainDeparture Instances to
  TrainDeparture trainDeparture1;
  TrainDeparture trainDeparture2;
  TrainDeparture trainDeparture3;

  Train train1;
  Train train2;
  Train train3;
  

  // Menu Choices
  private final int printInformationBoard = 1;
  private final int addDeparture = 2;
  private final int setTrackToDeparture = 3;
  private final int setDelayToDeparture = 4;
  private final int findDepartureWithId = 5;
  private final int findDepartureWithDestination = 6;
  private final int addTrain = 7;
  private final int setWagonCountToTrain = 8;
  private final int setSystemTime = 9;
  private final int exit = 10;



  /**
   * <p>Init method for initializing the different instances of the register calss.</p><br> 
   * <p>Aswell as the other necessary classes</p>
   */
  public void init() {
    departureRegister = new TrainDepartureRegister();
    trainRegister = new TrainRegister();
    exitProgram = false;
    systemTime = LocalTime.of(0, 0);

    trainDeparture1 = new TrainDeparture(123, "F1", "Oslo", LocalTime.of(12, 30));
    trainDeparture2 = new TrainDeparture(456, "F2", "Trondheim", LocalTime.of(12, 32));
    trainDeparture3 = new TrainDeparture(789, "F4", "Stavagner", LocalTime.of(12, 40));

    train1 = new Train(123, 5, "ICE");
    train2 = new Train(456, 10, "IC");
    train3 = new Train(789, 3, "Express");

    // addig the trains and trainDepartures to the corresponing registers
    departureRegister.addDeparture(trainDeparture1);
    departureRegister.addDeparture(trainDeparture2);
    departureRegister.addDeparture(trainDeparture3);

    trainRegister.addTrain(train1);
    trainRegister.addTrain(train2);
    trainRegister.addTrain(train3);
  }

  /** 
  * <p>Start method.</p>
  */
  public void start() {
    clearTerminal();
    System.out.println("Welcome to the train dispach system app");
    System.out.println("\n");
    menu();
    System.out.println("Thank you for using the train dispach system app");
    scanner.close();
    return;
  }

  /**
   * <p>Method to display the menu.</p>
   */
  public void displayMenu() {
    System.out.println("Please choose an option 1-8 from below "
        + "by typing in the number of the option: ");


    System.out.println("1. Print information board");
    System.out.println("2. Add train departure");
    System.out.println("3. Set track to train departure");
    System.out.println("4. Set delay to train departure");
    System.out.println("5. Find departure with id");
    System.out.println("6. Find departure with destination");
    System.out.println("7. Add train");
    System.out.println("8. Set wagon count to train");
    System.out.println("9. Set system time");
    System.out.println("10. Exit");
  }

  /**
   * <p>Method to show the menu and get the users choice to then execute.</p>
   * <p>the correct code that shows the correct result.</p>
   */
  public void menu() {
    while (exitProgram == false) {

      displayMenu();
      int choice = getInteger();

      clearTerminal();

      switch (choice) {
        case printInformationBoard:
          try {
            printInformationBoard();
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          break;
        case addDeparture:
          // Getting the input from the user
          System.out.println("Please enter the id of the train departure: ");
          int id = getInteger();
          System.out.println("Please enter the line number of the train departure (e.g F1): ");
          String line = getString();
          System.out.println("Please enter the destination of the train departure: ");
          String destination = getString();
          System.out.println("Please enter the departure time of the train departure: ");
          LocalTime departureTime = getTime();
          try {
            validateDepartureTime(departureTime, systemTime);
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }

          // Creating the departure
          try {
            TrainDeparture departure 
                = new TrainDeparture(id, line, destination, departureTime);
            departureRegister.addDeparture(departure);

            System.out.println("Departure added successfully");
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          break;
        case setTrackToDeparture:
          System.out.println("Please enter the id of the train departure: ");
          int trainNumber = getInteger();
          System.out.println("Please enter the track for the train departure: ");
          int trackNumber = getInteger();
          try {
            departureRegister.setTrackToDeparture(trainNumber, trackNumber);
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          break;


        case setDelayToDeparture:
          System.out.println("Please enter the id of the train departure: ");
          trainNumber = getInteger();
          System.out.println("Please enter the delay of the train departure in minutes: ");
          int delay = getInteger();
          try {
            departureRegister.setDelayToDeparture(trainNumber, delay);
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          break;

          
        case findDepartureWithId:
          System.out.println("Please enter the id of the train departure: ");
          trainNumber = getInteger();
          try {
            printHeader();
            System.out.println(departureRegister.getDepartureWithId(trainNumber)
                .toString());
            System.out.println(trainRegister.printInformation(trainNumber));

          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          System.out.println("\n\nPress Enter key to continue");
          scanner.nextLine();
          scanner.nextLine();
          break;


        case findDepartureWithDestination:
          System.out.println("Please enter the destination of the train departure: ");
          destination = getString();
          try {
            printHeader();
            departureRegister.getDeparturesWithDestination(destination)
                .forEach((key, departure) -> {
                  System.out.println(departure.toString());
                });
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          break;

        case addTrain:
          // get user input
          System.out.println("Please enter the train number of the train you want to ad:");
          trainNumber = getInteger();
          System.out.println("Please enter the wagon count of the train:");
          int wagonCount = getInteger();
          System.out.println("Please enter the type of the train:");
          String type = getString();

          try {
            Train train = new Train(trainNumber, wagonCount, type);
            trainRegister.addTrain(train);
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          break;


        case setWagonCountToTrain:
          // get user input
          System.out.println("Please enter the train number of the train you want to ad:");
          trainNumber = getInteger();
          System.out.println("Please enter the wagon count:");
          wagonCount = getInteger();

          try {
            trainRegister.setWagonCountToTrain(trainNumber, wagonCount);
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }

          break;
        case setSystemTime:
          LocalTime newSystemTime = getTime();
          try {
            updateClock(newSystemTime);
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
          break;
        case exit:
          exitProgram = true;
          break;
        default:
          break;
      }
    }
  }

  /**
   * <p>print informationBoardHeader.</p>
   */
  public void printHeader() {
    System.out.println(String.format("%-32s %-15s %-20s %-20s %-10s", 
        "Avngagner/Departures",
        "Desination",
        "Forsinkelse/Delay",
        "Spor/Track",
        "System time:" + systemTime.toString()));
    System.out.println("------------------------------------------------" 
        + "-------------------------------------------------------------");
  }

  /**
   * <p>Method to print the departures in the register.</p>
   */
  public void printInformationBoard() {
    printHeader();
    departureRegister.getSortedTrainDepatures().forEach((key, departure) -> {
      System.out.println(departure.toString());
    });
    System.out.println("\n\nPress Enter key to continue");
    scanner.nextLine();
    scanner.nextLine();
  }
  
  /**
   * <p>Method to get an Integer as input from the user. </p>
   * <p>Only returning when a valid Integer is entered.</p>

   * @return scanner.nextInt() the valid Integer
   */
  private int getInteger() {
    while (!scanner.hasNextInt()) {
      System.out.println("Please enter a valid number");
      scanner.next();
    }
    return scanner.nextInt();
  }

  /**
   * <p>Method to get a String as input from the user. </p>
   * <p>Only returning when a valid String is entered.</p>

   * @return scanner.nextLine() the valid String
   */
  private String getString() {
    // scanner.next(); // Consume the newline character
    while (!scanner.hasNext()) {
      System.out.println("Ugyldig inntasting. Skriv inn Tekst.");
      scanner.next(); // Consume the invalid input
    }
    return scanner.next();
  }

  /**
   * <p>Method to get a LocalTime as input from the user.</p>
   * <p>Only returning when a valid LocalTime is entered.</p>

   * @return time the valid LocalTime
   */
  private LocalTime getTime() {
    int hours = getHours();
    int minutes = getMinutes();
    LocalTime time = LocalTime.of(hours, minutes);
    return time;
  }

  /**
   * <p>Method to update the system time.</p>
   * <p>Should only be used when the system time is updated.</p>
   * <p>Should throw an exeption when the system time is invalid, </p>

   * @param newTime the new system time
   * @throws IlleagalArgumentException if the system time is invalid
   */
  public void updateClock(LocalTime newTime) 
      throws IllegalArgumentException {
    if (newTime.isBefore(systemTime)) {
      throw new IllegalArgumentException("System time cannot be set to a " 
          + "new time that is before the current time");
    }

    systemTime = newTime;

    try {
      departureRegister.removePartedDepartures(systemTime);        
    } catch (Exception e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  /**
   * <p>Validation method to check if the departure time is valid.</p>
   * <p>Should only be used when the departure time is entered by the user.</p>
   * <p>Should throw an exeption when the departure time is invalid, </p>
   * <p>meaning it is before the system time.</p>

   * @param departureTime the departure time to validate
   * @throws IlleagalArgumentException if the departure time is invalid
   */
  private void validateDepartureTime(LocalTime departureTime, LocalTime systemTime) {
    if (departureTime.isBefore(systemTime)) {
      throw new IllegalArgumentException("Departure time cannot be before system time");
    }
  }

  /* 
   * <p>Method to get valid hours for the departure time from the user. </p>
   * <p>Only returning when a valid hour is entered.</p>

   * @return hours the valid hours
   */
  private int getHours() {
    int hours;
    do {
      System.out.println("Please enter the amount of hours: ");
      hours = getInteger();
    } while (hours < 0 || hours > 23);
    return hours;
  }

  /**
   * <p>Method to get valid minutes for the departure time from the user.</p>

   * @return minutes the valid minutes
   */
  private int getMinutes() {
    int minutes;
    do {
      System.out.println("Please enter the amount of minutes: ");
      minutes = getInteger();
    } while (minutes < 0 || minutes > 59);
    return minutes;
  }

  static void clearTerminal() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
  }
}