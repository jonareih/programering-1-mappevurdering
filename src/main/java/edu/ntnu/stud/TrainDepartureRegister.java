package edu.ntnu.stud;

// imports
import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>Represents a departure register.</p>
 * <p>Goal: Execute the functionality of the menu with the TrainDeparture class</p>

 * @author Jonas Reiher
 * @version 1.0
 * @since 0.2
 * 
 */
public class TrainDepartureRegister {
  // HashMap containing the current departures
  private final HashMap<Integer, TrainDeparture> departures 
      = new HashMap<Integer, TrainDeparture>();
  
  /**
   * <p>GetMethod for departures.</p>
   *
   * @return the departures
   */
  public HashMap<Integer, TrainDeparture> getDepartures() {
    return departures;
  }

  /**
   * <p>Method for adding departures in the register.</p>

   * @param departure the departure to add
   * @throws IllegalArgumentException if the departure allready exists in the register or if
   another departure is arriving at the same time on the same track. 
   Also if the input is invalid the illeaegalArgumentException from the constructor in the 
   {@link TrainDeparture} class is thrown
   * 
   */
  public void addDeparture(TrainDeparture departure) throws IllegalArgumentException {
    // Check if departure allready exists in register
    if (departures.containsKey(departure.getTrainNumber())) {
      // return error message: departure trainNumber that alleady exists
      throw new IllegalArgumentException("Can't add depature since it allready exitst");
    }
    
    // add departure to register
    departures.put(departure.getTrainNumber(), departure);
    
  }

  /**
   * <p>Method for finding departure based in the given id.</p>
   * <p><br>Verifying that the inputs are valid with the methods:</p>
   * {@link #verifyPositiveNumber() verifyPositiveNumber()}<p><br></p>

   * @param trainNumber the trainNumber of the departure to find
   * @return the departure with the given trainNumber
   * @throws IllegalArgumentException if the trainNumber is negative
   */
  public TrainDeparture getDepartureWithId(int trainNumber) 
      throws IllegalArgumentException {
    verifyPositiveNumber(trainNumber, "TrainNumber");
    return departures.get(trainNumber);
  }

  /**
   * <p>Method for finding departures based on the given destination.</p>
   * <p><br>Verifying that the inputs are valid with the methods:</p>
   * {@link #verifyStringParameter() verifyStringParameter()}

   * @param destination the destination to find
   * @return an ArrayList with all the departures with the given destination
   */
  public HashMap<Integer, TrainDeparture> getDeparturesWithDestination(String destination) 
      throws IllegalArgumentException {

    // Verify that the input is valid
    verifyStringParameter(destination, "Destination");
    if (departures.isEmpty()) {
      throw new IllegalArgumentException("There are no entries in the register");
    }

    // Convert destination to lower case to avoid case sensitivity
    String lowerCaseDestination = destination.toLowerCase();

    // Use streams to filter and collect the desired departures
    HashMap<Integer, TrainDeparture> departuresWithDestination = departures.entrySet()
            .stream()
            .filter(entry ->
            entry.getValue().getDestination().toLowerCase().equals(lowerCaseDestination))
            .collect(Collectors.toMap(
                Map.Entry::getKey, 
                Map.Entry::getValue, 
                (entry1, enrty2) -> entry1, 
                LinkedHashMap::new));

    return departuresWithDestination;
  }

  /**
   * <p>Method for removing departures based on the given time.</p>
   * <p>Used Collection.removeIf() to remove the departures</p>
   * <p>Removes the departures where the current time is after the departure time plus the delay</p>
   * 
   * <p>Used Chat GPT tp help me fiks an error that came up with my solution,<br>
      that chat aws able to fiks by using removeIf(), istead of just remove()</p>

   * @param curTime the current system time defined by the user
   */
  public void removePartedDepartures(LocalTime curTime) {
    // This solution was given to me by Chat GPT 
    // because my solution gave an error about changing the amount of elements in a list 
    // while looping over it.
    departures.entrySet().removeIf(entry -> {
      // get the departure
      TrainDeparture curDeparture = entry.getValue();

      /// get the departure time and delay
      LocalTime departureTime = curDeparture.getDepartureTime();
      int departureDelay = curDeparture.getDelay();
      // calculate the new departure time
      LocalTime newDepartureTime = departureTime.plusMinutes(departureDelay);

      // return true if the current time is after the new departure time
      // which in turn removed the deparutre from the register
      return curTime.isAfter(newDepartureTime);
    });
  }
 
  /**
   * <p>Method for returning the depatures in the reigster sortet by departure time.</p>

   * @return an HashMap with all the departures in the register sorted by departure time
   */
  public HashMap<Integer, TrainDeparture> getSortedTrainDepatures() {
    // Using streams to sort the list of depatures
    if (departures.isEmpty()) {
      throw new IllegalArgumentException("There are no entries in the register");
    }
    HashMap<Integer, TrainDeparture> sortedDepartures 
        = departures.entrySet()
                    .stream()
                    .sorted(Comparator.comparing(entry -> entry.getValue().getDepartureTime()))
                    .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (entry1, entry2) -> entry1,
                        LinkedHashMap::new));
                                                                    
    return sortedDepartures;
  }

  /**
   * <p>Method for setting the track of the departure 
   * with the given trainNumber to the given track.</p>

   * @param trainNumber the trainNumber of the departure to set the track to
   * @param track the track to set the departure to
   * @throws IllegalArgumentException if another departure is arriving at the same time on the
   or if the track is negative
   */
  public void setTrackToDeparture(int trainNumber, int track) throws IllegalArgumentException {
    TrainDeparture departure = departures.get(trainNumber);

    if (departure == null) {
      throw new IllegalArgumentException("There is no departure with the given Train number");
    }
    // Check if another departure is arriving at the same time on the same track
    departures.entrySet().forEach(entry -> {
      if (entry.getValue().getTrack() == track 
            && entry.getValue().getDepartureTime().equals(departure.getDepartureTime())) {
        throw new IllegalArgumentException("Can't set track since another train is "
                                          + "arriving at the same time on the same track");
      }
    });
    departure.setTrack(track);
  }

  /**
   * <p>Method for setting the delay of the departure 
   * with the given trainNumber to the given delay.</p>

   * @param trainNumber the trainNumber of the departure to set the delay to
   * @param delay the delay to set the departure to
   * @throws IllegalArgumentException if the departure is null
   */
  public void setDelayToDeparture(int trainNumber, int delay) throws IllegalArgumentException {
    TrainDeparture departure = departures.get(trainNumber);

    if (departure == null) {
      throw new IllegalArgumentException("There is no departure with the given Train number");
    }
    departure.setDelay(delay);
  }

  /**
   * <p>Method for verifying that the input is a positive number.</p>

   * @param number the number to verify
   * @param parameterName the name of the parameter to use in the error message
   * @throws IllegalArgumentException if the input is negative with 
      the parameterName as an identifyer in the error message 
   */
  private void verifyPositiveNumber(int number, String parameterName)
      throws IllegalArgumentException {
    if (number < 0) {
      throw new IllegalArgumentException(parameterName + " cannot be negative");
    }
  }

  /**
   * <p>Method for verifying that the input is not a blank String.</p>

   * @param string the string to verify
   * @param parameterName the name of the parameter to use in the error message
   * @throws IllegalArgumentException if the input is blank with 
      the parameterName as an identifyer in the error message
   */
  private void verifyStringParameter(String string, String parameterName) 
      throws IllegalArgumentException {
    if (string.isBlank()) {
      throw new IllegalArgumentException(parameterName + " cannot be empty");
    }
  }

  
}
