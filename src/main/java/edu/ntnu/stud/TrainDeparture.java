package edu.ntnu.stud;

/*Importing the nessecary packages*/
import java.time.LocalTime;



/**
 * <p>The TrainDeparture class is used to represent a train departure.</p>
 * <p>Goal: act as a model for a departure, containing relevant information to that departure.</p>

 * @author Jonas Reiher
 * @version 1.0
 * @since 0.1 
 */
public class TrainDeparture  {

  // Implementing the variables trainId, linenumber, desitnation, track, departureTime and delay.

  private final int trainNumber;
  /*LineNumber is String because they often contain letters in them e.g. F1 */
  private final String lineNumber;
  private final String destination;
  private int track;
  /*
    * Implementing LocalTime for departureTime because it is a time of day.
    * Implementing int for delay because it is a number and 
    * Localtime has a function to add an amount of minutes to 
    * a LocalTime with the input value beeing an int.
    */
  // time of departure
  private final LocalTime departureTime;
  // delay in minutes
  private int delay;

  /**
    * <p>Contructor for the TrainDeparture class.</p>
    * <br><p>Goal: create a train departure with the given information</p>
    *
    * <p>Verifying that the inputs are valid with the methods:</p>
    * {@link #verifyPositiveNumber() verifyPositiveNumber()}<p><br></p>
    * {@link #verifyStringParameter() verifyStringParameter()}
    *
    * @param trainNumber the trainNumber of the train departure as a positive integer
    * @param line the line number of the train departure as a String
    * @param destination the destination of the train departure as a String
    * @param time the departure time of the train departure as a LocalTime

    * @throws IllegalArgumentException if any of the Strings are blank, 
      or if any of the numbers are negative
   
  */
  public TrainDeparture(int trainNumber, String line, String destination, LocalTime time) 
      throws IllegalArgumentException {

    /* Verifying that the inputs are valid */
    verifyPositiveNumber(trainNumber, "TrainNumber");
    verifyStringParameter(line, "Line");
    verifyStringParameter(destination, "Destination");
    

    /* Initializing the variables with the input values */
    this.trainNumber = trainNumber;
    this.lineNumber = line;
    this.destination = destination;
    this.departureTime = time;

    /* Initializing Delay with value 0 because the train does not initially have a delay
       and not checkig if delay is negativ because it aloows trains to be early */
    this.delay = 0;
  }

  /**
   * <p>Gets the id of the train departure.</p>

   * @return id of the train departure
   */
  public int getTrainNumber() {
    return this.trainNumber;
  }

  /**
   * <p>Gets the line number of the train departure.</p>

   * @return the line number of the train departure
   */
  public String getLineNumber() {
    return this.lineNumber;
  }

  /**
   * <p>Gets the destination of the train departure.</p>

   * @return the destonation of the train departure
   */
  public String getDestination() {
    return this.destination;
  }

  /**
   * <p>Gets the track of the train departure.</p>

   * @return the track of the train departure
   */
  public int getTrack() {
    return this.track;
  }


  /**
   * <p>Gets the departure time of the train departure.</p>

   * @return the departure time
   */
  public LocalTime getDepartureTime() {
    return this.departureTime;
  }

  /**
   * <p>Gets the delay of the train departure.</p>

   * @return the delay in minutes
   */
  public int getDelay() {
    return this.delay;
  }

  /**
   * <p>Sets the delay of the train departure to be the value of the parameter.</p>

   * @param delay the delay in minutes
   */
  public void setDelay(int delay) {
    this.delay = delay;
  }

  /**
   * <p>Sets the track of the train departure to be the value of the parameter.</p>
   * <br><p>Verifying that the input is valid with the method:</p>
   * {@link #verifyPositiveNumber() verifyPositiveNumber()}

   * @param track the track of the train departure
   */
  public void setTrack(int track) throws IllegalArgumentException {
    verifyPositiveNumber(track, "Track");
    this.track = track;
  }

  /**
   * <p>ToString method to make all the relevant information about the departure into a String.</p>

   * @return  all the information about the departure.
   */
  @Override
  public String toString() {
    String delayString = delay != 0 ? Integer.toString(delay) + " min" : "";
    String trackString = track != 0 ? Integer.toString(track)          : "";

    String returnString = String.format("%-10s %-10s %-10s %-15s %-20s %-10s",
        this.departureTime.toString(),
        this.lineNumber,
        this.trainNumber,
        this.destination,
        delayString,
        trackString);

    return returnString;
  }

  /**
   * <p>Method for verifying that the input is a positive number.</p>

   * @param number the number to verify
   * @param parameterName the name of the parameter to use in the error message
   * @throws IllegalArgumentException if the input is negative with 
      the parameterName as an identifyer in the error message 
   */
  private void verifyPositiveNumber(int number, String parameterName)
      throws IllegalArgumentException {
    if (number < 0) {
      throw new IllegalArgumentException(parameterName + " cannot be negative");
    }
  }

  /**
   * <p>Method for verifying that the input is not a blank String or any special characters.</p>

   * @param string the string to verify
   * @param parameterName the name of the parameter to use in the error message
   * @throws IllegalArgumentException if the input is blank with 
      the parameterName as an identifyer in the error message
   */
  private void verifyStringParameter(String string, String parameterName) 
      throws IllegalArgumentException {
    if (string.isBlank()) {
      throw new IllegalArgumentException(parameterName + " cannot be empty");
    }
  }
}
