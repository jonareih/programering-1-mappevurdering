package edu.ntnu.stud;

import java.util.HashMap;

/**
 * <p>This is the klass for the trainRegister.</p>
 * <p>Goal: Work with the DepartureRegister to display more specific information about a train</p>

 * @author Jonas Reiher
 * @version 1.0
 * @since 0.4
 */
public class TrainRegister {
  private HashMap<Integer, Train> trains;

  /**
   * <p>Constructor for the TrainRegister.</p>
   */
  public TrainRegister() {
    trains = new HashMap<Integer, Train>();
  }

  /** 
   * <p>Method to add new trains to the register.</p>

   * @param train the train object to add to the register
   * @throws IllegalArgumentException if the train allready exits
   */
  public void addTrain(Train train) {
    if (trains.containsKey(train.getTrainNumber())) {
      throw new IllegalArgumentException("Can't add a train that allready exists");
    }
    trains.put(train.getTrainNumber(), train);
  }

  /**
   * <p>Method to set the wagonCount of a train given the trainNumber.</p>

   * @param trainNumber the trainNumber of the train
   * @throws IllegalArgumentException if the new WagonCoutn is more than 12
   */
  public void setWagonCountToTrain(int trainNumber, int wagonCount) {
    if (!trains.containsKey(trainNumber)) {
      throw new IllegalArgumentException("The train with trainNumber: " 
          + trainNumber + " does not exist");
    }
    if (wagonCount > 12) {
      throw new IllegalArgumentException("Train can't have more than 12 wagons");
    }
    trains.get(trainNumber).setWagonCount(wagonCount);
  }

  /**
   * <p>method for returning a string for the UI class to print the information about a train.</p>

   * @param trainNumber the trainNumber of the train
  */
  public String printInformation(int trainNumber) {

    if (!trains.containsKey(trainNumber)) {
      throw new IllegalArgumentException("The train with trainNumber: " 
          + trainNumber + " does not exist");
    }
    
    String[] segments = {"A", "B", "C", "D", "E", "F"};
    Train train = trains.get(trainNumber);
    int trainWagonCount = train.getWagonCount();
    String segmentString = "Train: "    + train.getTrainNumber()
                         + " of type: " + train.getType() + "\n";
    for (int i = 0; i < trainWagonCount + 1; i++) { 
      if (i % 2 == 0 && i != 0) {
        segmentString += "Segment: " + segments[(i / 2) - 1] 
                       + " - Wagons " + (i - 1) 
                       + " and " + (i) + "\n";
      }
    } 
    if (trainWagonCount % 2 != 0) {
      int count = trainWagonCount - 1;
      int segmentIndex = count / 2;
      segmentString += "Segment: " 
                     + segments[segmentIndex] 
                     + " - Wagon " + trainWagonCount + "\n";
    }
    
    return segmentString;
  }
}