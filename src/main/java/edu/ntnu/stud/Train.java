package edu.ntnu.stud;

/**
 * <p>This is the klass for a train.</p>
 * <p>Goal: represent a train</p>

 * @author Jonas Reiher
 * @version 1.0
 * @since 0.4
 */
public class Train {
  private final int trainNumber;
  private int wagonCount;
  private final String type;

  /**
   * <p>Constructor for the train class.</p>
   * <p>Goal: create a train with the given information</p>

   * @param trainNumber the id of the train as a positive integer
   * @param wagonCount the wagon count of the train as a positive integer
   * @param type the type of the train as a String
   * @throws IllegalArgumentException if any of the Strings are blank, 
        or if any of the numbers are negative
    */
  public Train(int trainNumber, int wagonCount, String type) throws IllegalArgumentException {
    verifyPositiveNumber(trainNumber, "Id");
    verifyPositiveNumber(wagonCount, "Wagon count");
    if (wagonCount > 12) {
      throw new IllegalArgumentException("Train can't have more than 12 wagons");
    }
    verifyStringParameter(type, "Type");

    this.trainNumber = trainNumber;
    this.wagonCount = wagonCount;
    this.type = type;
  }

  /**
   * <p>Get Method for the Train Id witch mathes the id of the TrainDeparture.</p>

   * @return id the id of the train
   */
  public int getTrainNumber() {
    return this.trainNumber;
  }

  /**
   * <p>Get Method fo the wagon count.</p>

   * @return wagonCount the wagon count of the train
   */
  public int getWagonCount() {
    return wagonCount;
  }

  /**
   * <p>Get method for the type of the train.</p>

   * @return type the type of the Train
   */
  public String getType() {
    return this.type;
  }

  /**
   * <p>Set Method for the wagonCount.</p>

   * @param wagonCount the new count for the wagons
   * @throws IllegalArgumentException if the wagon count is negative
   */
  public void setWagonCount(int wagonCount) {
    verifyPositiveNumber(wagonCount, "wagonCount");
    if (wagonCount > 12) {
      throw new IllegalArgumentException("Train can't have more than 12 wagons");
    }
    this.wagonCount = wagonCount;
  }

  /**
   * <p>Method for verifying that the input is a positive number.</p>

   * @param number the number to verify
   * @param parameterName the name of the parameter to use in the error message
   * @throws IllegalArgumentException if the input is negative with 
      the parameterName as an identifyer in the error message 
   */
  private void verifyPositiveNumber(int number, String parameterName)
      throws IllegalArgumentException {
    if (number < 0) {
      throw new IllegalArgumentException(parameterName + " cannot be negative");
    }
  }

  /**
   * <p>Method for verifying that the input is not a blank String or any special characters.</p>

   * @param string the string to verify
   * @param parameterName the name of the parameter to use in the error message
   * @throws IllegalArgumentException if the input is blank with 
      the parameterName as an identifyer in the error message
   */
  private void verifyStringParameter(String string, String parameterName) 
      throws IllegalArgumentException {
    if (string.isBlank()) {
      throw new IllegalArgumentException(parameterName + " cannot be empty");
    }
    
  }
}