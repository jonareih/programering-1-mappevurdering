package edu.ntnu.stud;

/**
 * <p>This is the main class for the train dispatch application.</p>
 * <p>Goal: start the application</p>

 * @author Jonas Reiher
 * @version 1.0
 * @since 0.1
 */
public class TrainDispatchApp {
  
  private static UserInterface ui = new UserInterface();

  /**
   * <p>This is the main method.</p>
   */
  public static void main(String[] args) {
    ui.init();
    ui.start(); 
    System.exit(1);
  }

}