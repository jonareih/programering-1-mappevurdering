package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import java.util.HashMap;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for TrainDepartureRegister.
 */
public class TrainDepartureRegisterTest {

  /**
   * InnerTrainDepartureTest for positive Tests.
   */
  @Nested
  @DisplayName("Positive test for the DepartureRegister class")
  public class PositiveTests {

    /**
     * Test for the addDepartrure method. 
     * Should not throw an exception.
     */
    @Test
    void addingDepartureThatIsNotYetInTheRegister() {
      TrainDepartureRegister register = new TrainDepartureRegister();
      int departureId = 1;

      TrainDeparture departure 
          = new TrainDeparture(departureId, "F1", "Trondheim", LocalTime.of(12, 0));
      
      try {
        register.addDeparture(departure);
      } catch (Exception e) {
        fail("The test failed because the addDeparture method threw an exception" 
            + " with error message: " + e.getMessage());
      }
      assertEquals(departure, register.getDepartures().get(departureId));
    }

    /** 
     * Test for the addDeparture Method with multiple departures that all are not in the register 
     * and are not duplicates of eachother.
     * Should not throw an exception.
     */
    @Test
    void addingMultipleDeparturesThatAreNotInTheRegister() {
      TrainDepartureRegister register = new TrainDepartureRegister();

      TrainDeparture departure1 = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      TrainDeparture departure2 = new TrainDeparture(2, "F1", "Trondheim", LocalTime.of(12, 0));
      TrainDeparture departure3 = new TrainDeparture(3, "F1", "Trondheim", LocalTime.of(12, 0));
      
      try {
        register.addDeparture(departure1);
        register.addDeparture(departure2);
        register.addDeparture(departure3);
      } catch (Exception e) {
        fail("The test failed because the addDeparture method threw an exception " 
            + " with error message: " + e.getMessage());
      }

      assertTrue(register.getDepartures().containsKey(departure1.getTrainNumber()) 
          && register.getDepartures().containsKey(departure2.getTrainNumber()) 
          && register.getDepartures().containsKey(departure3.getTrainNumber()));

      assertEquals(departure1, register.getDepartures().get(departure1.getTrainNumber()));
      assertEquals(departure2, register.getDepartures().get(departure2.getTrainNumber()));
      assertEquals(departure3, register.getDepartures().get(departure3.getTrainNumber()));
    }

    /**
     * Test for the getDepartureWithId method. 
     * Should return the departure with the given id.
     */
    @Test
    void getDepartureWithIdOneShouldReturndepartureWithIdOne() {
      TrainDepartureRegister register = new TrainDepartureRegister();
      TrainDeparture departure = new TrainDeparture(1, "F4", "Oslo", LocalTime.of(12, 0));
      register.addDeparture(departure);

      assertEquals(departure, register.getDepartureWithId(departure.getTrainNumber()));
    }

    /**
     * Test for the getDeparturesWithDestination method. 
     * Should return the departures with the given destination.
     */
    @Test
    void getDeparturesWithDestinationTrondheimShouldReturndeparturesWithTrondheimAsDestination() {
      TrainDepartureRegister register = new TrainDepartureRegister();

      TrainDeparture departure1 = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      TrainDeparture departure2 = new TrainDeparture(2, "F1", "Trondheim", LocalTime.of(15, 0));
      TrainDeparture departure3 = new TrainDeparture(3, "F1", "Oslo", LocalTime.of(13, 0));
      
      register.addDeparture(departure1);
      register.addDeparture(departure2);
      register.addDeparture(departure3);

      HashMap<Integer, TrainDeparture> departuresWithDestinationTrondheim
          = register.getDeparturesWithDestination("Trondheim");
      assertTrue(departuresWithDestinationTrondheim.containsKey(1) 
          && departuresWithDestinationTrondheim.containsKey(2));
    }

    /**
     * Test for the removePartedDepartures method. 
     * Should remove the parted departures.
     */
    @Test
    void partedDeparturesShouldBeRemovedFromRegister() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F2", "Trondheim", LocalTime.of(14, 0));
      register.addDeparture(departure1);
      register.addDeparture(departure2);
      assertTrue(register.getDepartures().size() == 2);
      register.removePartedDepartures(LocalTime.of(13, 0));
      assertTrue(register.getDepartures().size() == 1);
      assertFalse(register.getDepartures().containsKey(departure1.getTrainNumber()));
    }

    /**
      * Test for the getSortedTrainDepatures method.
      * Should return the departures sorted by departure time.
      */
    @Test
    void getSortedTraindeparturesReturnsSotedDepartures() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F1", "Trondheim", LocalTime.of(15, 0));
      var departure3 = new TrainDeparture(3, "F1", "Oslo", LocalTime.of(13, 0));
      register.addDeparture(departure1);
      register.addDeparture(departure2);
      register.addDeparture(departure3);

      //sorted hashMap
      HashMap<Integer, TrainDeparture> sortedHashMap = new HashMap<Integer, TrainDeparture>();
      sortedHashMap.put(departure1.getTrainNumber(), departure1);
      sortedHashMap.put(departure3.getTrainNumber(), departure3);
      sortedHashMap.put(departure2.getTrainNumber(), departure2);

      // Call the method to get sorted departures
      HashMap<Integer, TrainDeparture> result = register.getSortedTrainDepatures();

      // Use assertions to verify that the result matches the expected sortedHashMap
      assertEquals(sortedHashMap, result);
    }

    /**
     * Test for the setTrackToDeparture method.
     * Should set the track to the given departure.
     * and not throw an exception.
     */
    @Test
    void settingTrackToDepartureShouldSetTheTrackToTheGivenDeparture() {
      var register = new TrainDepartureRegister();
      var departure = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      register.addDeparture(departure);

      int track = 1;
      try {
        register.setTrackToDeparture(departure.getTrainNumber(), track);
      } catch (Exception e) {
        fail("The test failed because the setTrackToDeparture method threw an exception" 
            + " with error message: " + e.getMessage());
      }
      assertEquals(track, departure.getTrack());
    }

    /**
     * Test fot the setDelayToDeparture method.
     * Should set the delay to the given departure.
     * and not throw an exception.
     */
    @Test
    void settingDelayToDepartureShouldSetTheDelayToTheGivenDeparture() {
      var register = new TrainDepartureRegister();
      var departure = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      register.addDeparture(departure);

      int delay = 10;
      try {
        register.setDelayToDeparture(departure.getTrainNumber(), delay);
      } catch (Exception e) {
        fail("The test failed because the setDelayToDeparture method threw an exception" 
            + "with error message: " + e.getMessage());
      }
      assertEquals(delay, departure.getDelay());
    }
  }


  /**
   * InnerTrainDepartureTest for negative Tests.
   */
  @Nested
  @DisplayName("Negative test for the DepartureRegister class")
  public class NegativeTests {

    /**
     * Test for the addDepartrure method. 
     * Should throw an exception.
     */
    @Test
    void addingDepartureThatIsInRegisterShouldNotAddToRegister() { 
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F1", "Trondheim S", LocalTime.of(12, 0));
      var departure3 = new TrainDeparture(3, "F1", "Trondheim S", LocalTime.of(12, 0));

      register.addDeparture(departure1);
      register.addDeparture(departure2);
      register.addDeparture(departure3);
      // departure with id that allready exitst
      var departure4 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(15, 0));
      try {
        register.addDeparture(departure4);
        fail("The test failed because the addDeparture method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Can't add depature since it allready exitst", e.getMessage());
      }
    }

    /** 
     * Test for the addDeparture Method with multiple departures that all are in the register
     * Should not throw an exception.
     */
    @Test
    void addingMultipleDeparturesThatAreInTheRegister() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F1", "Trondheim S", LocalTime.of(12, 0));
      var departure3 = new TrainDeparture(3, "F1", "Trondheim S", LocalTime.of(12, 0));

      register.addDeparture(departure1);
      register.addDeparture(departure2);
      register.addDeparture(departure3);

      var duplicateDeparture1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      var duplicateDeparture2 = new TrainDeparture(2, "F1", "Trondheim S", LocalTime.of(12, 0));
      var duplicateDeparture3 = new TrainDeparture(3, "F1", "Trondheim S", LocalTime.of(12, 0));
      try {
        register.addDeparture(duplicateDeparture1);
        register.addDeparture(duplicateDeparture2);
        register.addDeparture(duplicateDeparture3);
        fail("The test failed because the addDeparture method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Can't add depature since it allready exitst", e.getMessage());
      }

      assertNotEquals(register.getDepartures().get(1), duplicateDeparture1);
      assertNotEquals(register.getDepartures().get(2), duplicateDeparture2);
      assertNotEquals(register.getDepartures().get(3), duplicateDeparture3);
    }

    /**
     * Test for the getDepartureWithId method with a negative id as Input
     * Should throw an exeption.
     */
    @Test
    void getDepartureWithNegativeIdShouldThrowExeption() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F1", "Trondheim S", LocalTime.of(12, 0));
      var departure3 = new TrainDeparture(3, "F1", "Trondheim S", LocalTime.of(12, 0));
      register.addDeparture(departure1);
      register.addDeparture(departure2);
      register.addDeparture(departure3);

      try {
        register.getDepartureWithId(-1);
        fail("The test failed because the getDepartureWithId method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Id cannot be negative", e.getMessage());
      }
    }

    /**
     * Testing for case sensitivity in getDepartureWithDestination.
     * Should return the departures with the given destination.
     */
    @Test
    void testingForCaseSensitivityInGetDepaturesWithDestiniation() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F1", "Trondheim", LocalTime.of(15, 0));
      var departure3 = new TrainDeparture(3, "F1", "Oslo", LocalTime.of(13, 0));
      register.addDeparture(departure1);
      register.addDeparture(departure2);
      register.addDeparture(departure3);

      try {
        HashMap<Integer, TrainDeparture> departuresWithDestinationOslo
            = register.getDeparturesWithDestination("oSLo");

        assertTrue(departuresWithDestinationOslo.containsKey(3));
        assertEquals(departure3, departuresWithDestinationOslo.get(3));
      } catch (Exception e) {
        fail("The test failed because the getDeparturesWithDestination method threw an exception"
            + " with error message: " + e.getMessage());
      }
      
    }

    /**
     * Testinge getDeparturesWithDestination with a destination that is blank.
     * Should throw an exception.
     */
    @Test
    void getDeparturesWithDestinationWithBlankDestinationShouldThrowException() {
      var register = new TrainDepartureRegister();

      var departure1 = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F1", "Trondheim", LocalTime.of(15, 0));
      var departure3 = new TrainDeparture(3, "F1", "Oslo", LocalTime.of(13, 0));
      
      register.addDeparture(departure1);
      register.addDeparture(departure2);
      register.addDeparture(departure3);

      try {
        register.getDeparturesWithDestination("");
        fail("The test failed because the getDeparturesWithDestination method"
            + " did not throw an exception");
      } catch (Exception e) {
        assertEquals("Destination cannot be empty", e.getMessage());
      }
    }

    /**
     * Test for the getDeparturesWithDestination method while the register is empty.
     * Should throw an exception.
     */
    void getDeparturesWithDestinationInEmptyRegisterShouldThrowException() {
      var register = new TrainDepartureRegister();
      try {
        register.getDeparturesWithDestination("Trondheim");
        fail("The test failed because the getDeparturesWithDestination method"
            + " did not throw an exception");
      } catch (Exception e) {
        assertEquals("There are no entries in the register", e.getMessage());
      }
    }

    /**
     * Test for sorting the departures in an empty register.
     * Should throw an exception that there are no entries in the register.
     */
    @Test
    void sortingDeparturesInEmptyRegisterShouldThrowException() {
      var register = new TrainDepartureRegister();
      try {
        register.getSortedTrainDepatures();
        fail("The test failed because the getSortedTrainDepatures method"
            + " did not throw an exception");
      } catch (Exception e) {
        assertEquals("There are no entries in the register", e.getMessage());
      }
    }

    /**
     * Test for the setTrackToDeparture method with a negative track as input.
     * Should throw an exception.
     */
    @Test
    void settingTrackToNegativValueShouldThrowAndExeption() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      register.addDeparture(departure1);

      try {
        register.setTrackToDeparture(1, -1);
        fail("The test failed because the setTrackToDeparture method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Track cannot be negative", e.getMessage());
      }
    }

    /**
     * Test for the setTrackToDeparture method with a departure that is not in the register.
     * Should throw an exception.
     */
    @Test
    void settingTrackToDepartureThatIsNotInTheRegisterShouldThrowAndExeption() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      register.addDeparture(departure1);

      try {
        register.setTrackToDeparture(2, 1);
        fail("The test failed because the setTrackToDeparture method did not throw an exception");
      } catch (Exception e) {
        assertEquals("There is no departure with the given id", e.getMessage());
      }
    }

    /**
     * Test for setting a track to a departure that is arriving at the same time on the same track 
     * as another departure.
     * Should throw an exception.
     */
    @Test
    void settingTrackToDepartureThatIsArrivingAtTheSameTimeOnTheSameTrackAsAnotherDepartureShouldThrowException() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      var departure2 = new TrainDeparture(2, "F1", "Trondheim S", LocalTime.of(12, 0));
      register.addDeparture(departure1);
      register.addDeparture(departure2);

      int track = 1;

      departure1.setTrack(track);
      try {
        register.setTrackToDeparture(departure2.getTrainNumber(), track);
        fail("The test failed because the setTrackToDeparture method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Can't set track since another train is arriving"
            + " at the same time on the same track", e.getMessage());
      }
    }

    /**
     * Test for the setDelayToDeparture method with a non existing TrainDeparture.
     * should throw an exception.
     */
    @Test
    void settingDelayToDepartureThatIsNotInTheRegisterShouldThrowAndExeption() {
      var register = new TrainDepartureRegister();
      var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
      register.addDeparture(departure1);

      try {
        register.setDelayToDeparture(2, 1);
        fail("The test failed because the setDelayToDeparture method did not throw an exception");
      } catch (Exception e) {
        assertEquals("There is no departure with the given id", e.getMessage());
      }
    }
  }
}