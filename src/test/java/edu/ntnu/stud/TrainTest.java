package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


/**
 * Test class for the Train class.
 */
public class TrainTest {

  /**
   * Positive tests for the Train class.
   */
  @Nested
  @DisplayName("Positive tests")
  public class PositiveTests {

    /**
     * Positive test for the constructor. Should not throw an exception.
     */
    @Test
    void constructorTestShouldNotThrowAnException() {
      try {
        var train = new Train(1, 0, "PassengerTrain");
        assertNotNull(train);
      } catch (Exception e) {
        fail("The test failed because the constructor threw an exception " 
            + "with error message: " + e.getMessage());
      }
    }

    /**
     * Test the setWagonCount methood should nit throw an exception.
     */
    @Test
    void settingWagonCountTo5ShouldSetTheWagonCountTo5() {
      Train train = new Train(1, 0, "PassengerTrain");
      try {
        train.setWagonCount(5);
        
      } catch (Exception e) {
        fail("The test failed because the setWagonCount method threw an exception "
            + "with error message: " + e.getMessage());
      }
      assertEquals(5, train.getWagonCount());
    }
  } 


  /**
   * Negative tests for the Train class.
   */
  @Nested
  @DisplayName("Negative tests")
  public class NegativeTests {  
    /**
     * Negative test for the constructor. 
     * Testing with a negative id. Should throw an exception.
     * 
     */
    @Test
    void constructorTestShouldThrowAnExceptionWithNegativeId() {
      try {
        Train train = new Train(-1, 0, "TestType");
        fail("The test failed because the constructor did not throw an exception");
      } catch (Exception e) {
        assertEquals("Id cannot be negative", e.getMessage());
      }
    }
  
    /**
     * Negative test for the constructor.
     * Testing with a negative wagonCount. Should throw an exception.
     * This test is writen by GitHub copilot since its the same as the one above.
     * I chould have just copied the test and changed the values.
     * But copilot also changed the text for me so it was just more effective.
     */
    @Test
    void constructorTestShouldThrowAnExceptionWithNegativeWagonCount() {
      try {
        Train train = new Train(1, -1, "TestType");
        fail("The test failed because the constructor did not throw an exception");
      } catch (Exception e) {
        assertEquals("Wagon count cannot be negative", e.getMessage());
      }
    }  

    /**
     * Negative test for the constructor.
     * Testing with a blank type. Should throw an exception.
     * This test is writen by GitHub copilot since its the same as the one above.
     * I chould have just copied the test and changed the values.
     * But copilot also changed the text for me so it was just more effective.
     */
    @Test
    void constructorTestShouldThrowAnExceptionWithBlankType() {
      try {
        Train train = new Train(1, 1, "");
        fail("The test failed because the constructor did not throw an exception");
      } catch (Exception e) {
        assertEquals("Type cannot be empty", e.getMessage());
      }
    }

    /**
     * Negative test for the constuctor.
     * Testing with a wagonCount greater than 12. Should throw an exception.
     */
    @Test
    void constructorTestShouldThrowAnExceptionWithWagonCountGreaterThan12() {
      try {
        Train train = new Train(1, 13, "PassengerTrain");
        fail("The test failed because the constuctor did not throw an exception");
      } catch (Exception e) {
        assertEquals("Train can't have more than 12 wagons", e.getMessage());
      }
    }

    /**
     * Negative test for the setWagonCount method.
     * Testing with a negative wagonCount. Should throw an exception.
     */
    @Test
    void settingWagonCountToNegativeShouldThrowAnException() {
      Train train = new Train(1, 0, "PassengerTrain");
      try {
        train.setWagonCount(-1);
        fail("The test failed because the setWagonCount method did not throw an exception");
      } catch (Exception e) {
        assertEquals("wagonCount cannot be negative", e.getMessage());
      }
    }

    /**
     * Negative test for the setWagonCount method.
     * Testing with a wagonCount greater than 12. Should throw an exception.
     */
    @Test
    void settingWagonCountToValueMoreThan12ShouldThrowAnException() {
      Train train = new Train(1, 0, "PassengerTrain");
      try {
        train.setWagonCount(13);
        fail("The test failed because the setWagonCount method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Train can't have more than 12 wagons", e.getMessage());
      }
    }
  
  }
}
