package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


/**
 * Test class for the TrainRegister class.
 */
public class TrainRegisterTest {
    
  /**
   * positive tests for the TrainRegister class.
   */
  @Nested
  @DisplayName("Positive tests for the TrainRegister class")
  public class PositiveTests {

    /**
     * Test method for the addTrain method.
     * Should not throw an Exception
     */
    @Test
    void addingTrainToRegisterShouldNotThrowAnException() {
      var register = new TrainRegister();
      var train = new Train(1, 10, "TestType");
      try {
        register.addTrain(train);
      } catch (Exception e) {
        fail("The test feiled because the method threw an error with message "
            + e.getMessage());     
      }
    }

    /**
     * Test method for the setWagonCountToTrain.
     * Should not throw an exception
     */
    @Test
    void settingWagonCountTo10ToTheTrainWithId123ShouldSetTheWagonCountOfThatTrainTo10() {
      var register = new TrainRegister();
      var train = new Train(123, 0, "Test Type");
      
      register.addTrain(train);

      try {
        register.setWagonCountToTrain(123, 10);
      } catch (Exception e) {
        fail("The test failed because the test throw an error with error message "
            + e.getMessage());
      }
      assertEquals(10, train.getWagonCount());
    }

    /**
     * Test for the printInformation method.
     * Should return the expected result with even wagon count
     */
    @Test
    void printInformationShouldPrintTheExpectedResultWithEvenWagonCount() {
      var register = new TrainRegister();
      var train = new Train(123, 8, "TestType");
      register.addTrain(train);

      String result = register.printInformation(123);

      String expected = "Train: 123 of type: TestType\n"
                        + "Segment: A - Wagons 1 and 2\n"
                        + "Segment: B - Wagons 3 and 4\n"
                        + "Segment: C - Wagons 5 and 6\n"
                        + "Segment: D - Wagons 7 and 8\n";
      assertEquals(expected, result);
    }

    /**
     * Test for the printInformation method.
     * Should return the expected result with odd wagon count
     */
    @Test
    void printInformationShouldPrintTheExpectedResultWithOddWagonCount() {
      var register = new TrainRegister();
      var train = new Train(123, 7, "TestType");
      register.addTrain(train);

      String result = register.printInformation(123);

      String expected = "Train: 123 of type: TestType\n"
                        + "Segment: A - Wagons 1 and 2\n"
                        + "Segment: B - Wagons 3 and 4\n"
                        + "Segment: C - Wagons 5 and 6\n"
                        + "Segment: D - Wagon 7\n";
      assertEquals(expected, result);
    }

  }

  /**
   * Negative tests for the TrainRegisterTest.
   */
  @Nested
  @DisplayName("Negativ tests for the TrainRegister class")
  public class NegativeTests {
    
    /**
     * Test for the addTrain method.
     * Should throw an exception
     */
    @Test
    void addingTrainToRegisterThatAllreadyExitstShouldThrowAnException() {
      var register = new TrainRegister();
      var train = new Train(123, 8, "TestType");
      register.addTrain(train);

      var duplicateTrain = new Train(123, 8, "TestType");
      
      try {
        register.addTrain(duplicateTrain);
        fail("The test failed because the method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Can't add a train that allready exists", e.getMessage());
      }
    }

    /**
     * Test for the setWagonToTrain method with a train that does not exist.
     * Should throw an exception.
     */
    @Test
    void settingWagonCountToTrainThatIsNotInRegister() {
      var register = new TrainRegister();

      try {
        register.setWagonCountToTrain(123, 5);
        fail("The test failes because the method did not throw an exception");
      } catch (Exception e) {
        assertEquals("The train with id: 123 does not exist", e.getMessage());
      }
    }

    /**
     * Test for the setWagonToTrain method with a wagonCount greater than 12.
     * Should throw an exception.
     */
    @Test
    void settingWagonCountTo13ShouldThrowAnException() {
      var register = new TrainRegister();
      var train = new Train(123, 8, "TestType");
      register.addTrain(train);


      try {
        register.setWagonCountToTrain(123, 13);
        fail("The test failes because the method did not throw an exception");
      } catch (Exception e) {
        assertEquals("Train can't have more than 12 wagons", e.getMessage());
      }
    }

    /**
     * Test for the printInformation method with a train that does not exist.
     * Should throw an exception.
     */
    @Test
    void printInformationWithTrainNotInRegisterShouldThrowAnException() {
      var register = new TrainRegister();

      try {
        register.printInformation(123);
        fail("The test failes because the method did not throw an exception");
      } catch (Exception e) {
        assertEquals("The train with id: 123 does not exist", e.getMessage());
      }
    }
  }
}
