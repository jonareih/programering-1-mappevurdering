package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


/**
 * Test class for TrainDeparture.
 */
public class TrainDepartureTest {
  
  /**
   * InnerTrainDepartureTest for positive Tests.
   */
  @Nested
  @DisplayName("Positive test for the TrainDeparture class")
  
  public class PositiveTests {
    /**
     * Test for the constructor. Should not throw an exception.
     */
    @Test
    
    void constructorTestShouldNotGiveAnExeption() {
      try {
        var departure = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
        assertNotNull(departure);
      } catch (Exception e) {
        fail("The test failed because the constructor threw an exception"
            + " with error message: " + e.getMessage());
      }
    }

    /**
     * Test for the getId method. Should return the id.
     */
    @Test
    
    void getIdMethodShouldReturnTheIdInTheDeparture() {
      int trainNumber = 1;
      var departure = new TrainDeparture(trainNumber, "F1", "Trondheim", LocalTime.of(12, 0));
      assertEquals(1, departure.getTrainNumber());
    }

    /*
     * Test for the getLineNumber method. Should return the line number.
     */
    @Test
    void getLineNumberMethodShouldReturnTheLineNumberInTheDeparture() {
      String line = "F1";
      var departure = new TrainDeparture(1, line, "Trondheim", LocalTime.of(12, 0));
      assertEquals(line, departure.getLineNumber());
    }

    /*
     * Test for the getDestination method. Should return the destination.
     */
    @Test
    void getDestinationMethodShouldReturnTheDestinationInTheDeparture() {
      String destination = "Trondheim";
      var departure = new TrainDeparture(1, "F1", destination, LocalTime.of(12, 0));
      assertEquals(destination, departure.getDestination());
    }

    /*
     * Test for the getTrack method. Should return the track.
     */
    @Test
    void getTrackMethodShouldReturnTheTrackInTheDeparture() {
      int track = 1;
      var departure = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      departure.setTrack(track);
      assertEquals(track, departure.getTrack());
    }

    /*
     * Test for the getDepartureTime method. Should return the departure time.
     */
    @Test
    void getDepartureTimeMethodShouldReturnTheDepartureTimeInTheDeparture() {
      LocalTime departureTime = LocalTime.of(12, 0);
      var departure = new TrainDeparture(1, "F1", "Trondheim", departureTime);
      assertEquals(departureTime, departure.getDepartureTime());
    }

    /*
     * Test for the getDelay method. Should return the delay. 
     * Testing without setting the delay to something as that test has not beein cleared yet.
     */
    @Test
    void getDelayMethodShouldReturnZeroAsTheDelayIsSetToZeroWhenTheDepartureIsCreated() {
      int expected = 0;
      var departure = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      assertEquals(expected, departure.getDelay());
    }

    /*
     * Test for the setDelay method. Should set the delay to the param value.
     */
    @Test
    void setDelayMethodShouldSetTheDelayInTheDepartureToTheGivenValue() {
      int delay = 10;
      var departure = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      departure.setDelay(delay);
      assertEquals(delay, departure.getDelay());
    }

    /*
     * Test for setting the delay to a negative value. 
     * Should be set to a negatve value simulating the train beeing ahead of schedule.
     */
    @Test
    void settingTheDelayToNegativIntShouldSetTheDelayToNegativeValue() {
      int delay = -10;
      var departure = new TrainDeparture(1, "F1", "Trondheim", LocalTime.of(12, 0));
      departure.setDelay(delay);
      assertEquals(delay, departure.getDelay());
    }

    /*
     * Test for the toString method. 
     * Should return a string with all the relevant information in the departure.
     */
    @Test
    void toStringMethodShouldReturnStringWithAllTheRelevantInformationInTheDeparture() {
      int id = 1;
      String line = "F1";
      String destination = "Trondheim";
      int track = 1;
      LocalTime departureTime = LocalTime.of(12, 0);
      int delay = 10;

      var departure = new TrainDeparture(id, line, destination, departureTime);
      departure.setTrack(track);
      departure.setDelay(delay);
      String expected = departureTime 
          + "      " + line 
          + "         " + id 
          + "          " + destination 
          + "       " + delay + " min            " 
          +              id 
          + "         ";
      
      assertEquals(expected, departure.toString());
    }
  }


  /**
   * InnerTrainDepartureTest for negative Tests.
   */
  @Nested
  @DisplayName("Negative test for the TrainDeparture class")
  public class NegativeTests {

    /*
    * Test for id beeing negative.
    */
    @Test
    void idIsNegativeShouldThrowAnExeption() {
      try {
        var departure1 = new TrainDeparture(-1, "F1", "Trondheim S", LocalTime.of(12, 0));
        assertNotNull(departure1);
        fail("The test feiled because the constructor did not throw an exception");
      } catch (IllegalArgumentException e) {
        assertEquals("TrainNumber cannot be negative", e.getMessage());
      }
    }
    
    /*
    * Test for line beeing blank
    */
    @Test
    void lineIsBlankShouldThrowAnExeption() {
      try {
        var departure1 = new TrainDeparture(1, "", "Trondheim S", LocalTime.of(12, 0));
        assertNotNull(departure1);
        fail("The test failed because the constructor did not throw an exception");
      } catch (Exception e) {
        assertEquals("Line cannot be empty", e.getMessage());
      }
    }

    /*
    * Test for destination beeing blank
    */
    @Test
    void desinationIsBlankShouldThrowAnExeption() {
      try {
        var departure1 = new TrainDeparture(1, "F1", "", LocalTime.of(12, 0));
        assertNotNull(departure1);
        fail("The test failed because the constructor did not throw an exception");
      } catch (Exception e) {
        assertEquals("Destination cannot be empty", e.getMessage());
      }
    }

    /*
    * Test for track beeing negative
    */
    @Test
    void trackIsNegativeShouldThrowAnExeption() {
      try {
        var departure1 = new TrainDeparture(1, "F1", "Trondheim S", LocalTime.of(12, 0));
        assertNotNull(departure1);
        departure1.setTrack(-1);
        fail("The test failed because the constructor did not throw an exception");
      } catch (Exception e) {
        assertEquals("Track cannot be negative", e.getMessage());
      }
    }
  }
}